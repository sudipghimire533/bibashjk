let ready = null;

function queryClass(el, cls) {
	return el.getElementsByClassName(cls)[0];
}
class Track {
	constructor(title, album, slug) {
		this.title = title;
		this.album = album;
		this.slug = slug;
		this.duration = "";
	}
}
(function () {
	const cover_dir = "covers/";
	const audio_dir = "songs/";
	const all_tracks_info = [
		new Track(
			'Sochi rahe mah',
			'Umer satra', 'sochirahey'
		),
		new Track(
			'Samjhana',
			'Umer satra', 'samjana'
		),
		new Track(
			'Thik chaina',
			'Umer satra', 'thik-chaina'
		),
		new Track(
			'Badulki',
			'Umer satra', 'badulki'
		),
		new Track(
			'Bujhera bol',
			'Umer satra', 'bujhera-bol'
		),
		new Track(
			'Maichyang yo maichyang',
			'Chill vibes', 'maichang'
		),
		new Track(
			'Kulli - the title',
			'Chill vibes', 'kulli'
		)
	];

	let tracks = new Array(all_tracks_info.length);
	let players = new Array(all_tracks_info.length);
	let durations = new Array(all_tracks_info.length);
	let update_now = false;

	function get_current_playing_index() {
		let a = document.querySelector('.track.playing');
		return a === null ? -1 : parseInt(a.getAttribute('aria-index'));
	}

	function get_next_track_index() {
		return (get_current_playing_index() + 1) % players.length;
	}

	function get_prev_track_index() {
		return (get_current_playing_index() - 1) % players.length;
	}

	let track_stat_icon = null;
	let header_stat_icon = null;

	function set_icon(to, index = get_current_playing_index()) {
		track_stat_icon = queryClass(tracks[index], 'stat_icon');
		header_stat_icon = document.querySelector('.header .stat_icon');
		if (to === "play") {
			track_stat_icon.classList.remove('pause');
			track_stat_icon.classList.add('play');
			header_stat_icon.classList.remove('pause');
			header_stat_icon.classList.add('play');
		} else if (to === "pause") {
			track_stat_icon.classList.remove('play');
			track_stat_icon.classList.add('pause');
			header_stat_icon.classList.remove('play');
			header_stat_icon.classList.add('pause');
		}
	}

	function set_playing(index = get_current_playing_index()) {
		let header = document.querySelector('.header');
		queryClass(header, 'track_name').innerText = all_tracks_info[index].title;
		queryClass(header, 'album_name').innerText = all_tracks_info[index].title;
		queryClass(header, 'track_duration').innerText = all_tracks_info[index].duration;
		queryClass(header, 'cover_img').setAttribute('src', cover_dir + all_tracks_info[index].slug + '.jpg');
		set_icon("pause", index);
		document.querySelector('.jk_player .background').style.background = "url('" + cover_dir + all_tracks_info[index].slug + ".jpg')";
	}

	function play_audio(index) {
		console.log("Selcted index: " + index + "  Active index: " + get_current_playing_index());
		if (players[index].playing()) { // if this track is playing
			players[index].pause();
		} else if (get_current_playing_index() === index) { // if this track is active but paused
			players[index].play();
		} else {
			Howler.stop();
			players[index].play();
		}
	}

	function fill_tracks() {
		let template = queryClass(document, 'track');
		let target = template.parentElement;
		let working = null;
		all_tracks_info.forEach((tr, i) => {
			working = template.cloneNode(true);
			queryClass(working, 'track_name').innerText = tr.title;
			queryClass(working, 'album_name').innerText = tr.album;
			queryClass(working, 'cover_img').setAttribute('src', cover_dir + tr.slug + '.jpg');
			working.title = "Listen to " + tr.title + " from album " + tr.album;
			working.setAttribute('aria-index', i);
			target.appendChild(working);
			tracks[i] = working;
		});
		template.remove();

	}

	let slider, played_duration_el;
	let insr, cr_sto;

	function update_control() {
		if (update_now) {
			cr_sto = get_current_playing_index();
			insr = players[cr_sto].seek();
			played_duration_el.innerText =
				parseInt(insr / 60).toLocaleString('en-US', {
					minimumIntegerDigits: 2,
					useGrouping: false
				}) +
				':' +
				parseInt((insr - parseInt(insr / 60) * 60) % 60).toLocaleString('en-US', {
					minimumIntegerDigits: 2,
					useGrouping: false
				});
			slider.value = insr * 100 / durations[cr_sto];
		}
		setTimeout(update_control, 1000);
	}

	function initilize_controls() {
		document.querySelectorAll('.play_next').forEach(el => {
			el.addEventListener('click', function () {
				play_audio(get_next_track_index());
			});
		});
		document.querySelectorAll('.play_prev').forEach(el => {
			el.addEventListener('click', function () {
				play_audio(get_prev_track_index());
			});
		});
		slider = document.querySelector('.header .slider input');
		played_duration_el = document.querySelector('.header .played_duration');
		slider.value = 0.00;
		slider.addEventListener('input', function () {
			let val = parseInt(slider.value);
			let current = get_current_playing_index();
			players[current].seek((val / 100) * durations[current]);
		});

		let track_list = queryClass(document, 'list-track');
		queryClass(document, 'scroll_track').addEventListener('click', function () {
			track_list.scrollTop += 100;
		});

		update_control();
	}


	function initilize_audio() {
		all_tracks_info.forEach((it, index) => {
			let p = new Howl({
				src: [audio_dir + it.slug + '.webm', audio_dir + it.slug + '.mp3'],
				format: ['webm', '.mp3'],
				loop: false,
				autoplay: false,
				volume: 1,
				html5: true
			});
			p.on("play", function () {
				tracks[index].classList.add('playing');
				set_playing(index);
				update_now = true;
			});
			p.on("pause", function () {
				update_now = false;
				set_icon("play", index);
			});
			p.on("stop", function () {
				update_now = false;
				if (get_current_playing_index() === index) {
					set_icon("play", index);
					tracks[index].classList.remove('playing');
					played_duration_el.innerText = "0:0";
					set_icon("play", index);
				}
				slider.value = 0.00;
			});
			p.on("end", function () {
				play_audio(get_next_track_index());
			});
			p.once("load", function () {
				let dur = p.duration();
				durations[index] = dur;
				all_tracks_info[index].duration = parseInt(dur / 60).toLocaleString('en-US', {
					minimumIntegerDigits: 2,
					useGrouping: false
				}) + ':' + parseInt(dur % 60).toLocaleString('en-US', {
					minimumIntegerDigits: 2,
					useGrouping: false
				});
				// make able to play audio only when loaded
				queryClass(tracks[index], 'stat_icon').classList.remove('buff');
				if (index === 0) {
					dur = document.querySelector('.header .stat_icon');
					dur.classList.remove('buff');
					let cr = true;
					dur.addEventListener('click', function () {
						cr = get_current_playing_index();
						if (cr >= 0)
							play_audio(cr);
						else
							play_audio(0);
					});
				}
				tracks[index].addEventListener('click', function () {
					play_audio(index);
				});

			});
			p.on("playerror", function (id, err) {
				alert("Error while playing that track...");
				console.log(err);
			});
			players[index] = p;
		});
	}

	function make_social_links() {
		let cloned = queryClass(document, 'external_links').cloneNode(true);
		cloned.classList.remove('mobile');
		cloned.classList.add('desktop');
		queryClass(document, 'jk_player').prepend(cloned);
	}
	let note = null;

	function notify(msg, dur = 5) {
		note.innerText = msg;
		note.classList.add('show');
		setTimeout(function () {
			note.classList.remove('show');
		}, dur * 1000);
	}

	ready = function () {
		ready = null;
		make_social_links();
		fill_tracks();
		set_playing(0);
		set_icon("play", 0); // but dont put pause sign in the track itself
		initilize_audio();
		initilize_controls();
		// time to remove the loader
		queryClass(document, 'loader').remove();
		setTimeout(function () {
			note = queryClass(document, 'notes');
			notify('Album UMER SATRA is being updated in youtube...', 10);
		}, 7000);
	};
	
})();

